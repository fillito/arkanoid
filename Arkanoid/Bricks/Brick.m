//
//  Brick.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import "Brick.h"
@interface Brick()
@property (assign,nonatomic) NSUInteger receivedCollisions;
@property (weak,nonatomic) UILabel *collisionsCountLabel;
@end

static const CGFloat kMaxCollisions=1;
NSString *const brickWasDestroyed=@"brickWasDestroyed";

@implementation Brick

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 44, 15)];
    if (self) {
        // Initialization code
        self.backgroundColor=colorWithRGB(180, 180, 180);
        UILabel *collisionsCountLabel=[[UILabel alloc]initWithFrame:self.bounds];
        collisionsCountLabel.backgroundColor=self.backgroundColor;
        collisionsCountLabel.textColor=[UIColor whiteColor];
        collisionsCountLabel.text=@"0";
        collisionsCountLabel.textAlignment=NSTextAlignmentCenter;
        [self addSubview:collisionsCountLabel];
        self.collisionsCountLabel=collisionsCountLabel;
        self.collisionsCountLabel.hidden=YES;        
    }
    return self;
}

- (id)init{
    return [self initWithFrame:CGRectZero];
}
- (NSUInteger)collisions{
    return _receivedCollisions;
}
- (NSUInteger)maxCollisions{
    return kMaxCollisions;
}
- (void)collideWithBall:(Ball *)ball{
    self.receivedCollisions=self.receivedCollisions+1;
    self.collisionsCountLabel.text=[NSString stringWithFormat:@"%d",self.receivedCollisions];
    [self updateBackgroundDueCollisions];
}
- (void)updateBackgroundDueCollisions{
    CGFloat red;
    CGFloat green;
    CGFloat blue;
    CGFloat alpha;
    [self.backgroundColor getRed:&red green:&green blue:&blue alpha:&alpha];
    self.backgroundColor=colorWithRGBA(red, green, blue, 1.0-(self.collisions/(float)self.maxCollisions));
}
- (void)setReceivedCollisions:(NSUInteger)receivedCollisions{
    _receivedCollisions=receivedCollisions;
    if (_receivedCollisions>=self.maxCollisions) {
        [self destroy];
    }
}
- (void)destroy{
    [self removeFromSuperview];
    [[NSNotificationCenter defaultCenter]postNotificationName:brickWasDestroyed object:self userInfo:nil];
}
@end
