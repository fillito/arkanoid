//
//  LightBrick.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import "LightBrick.h"

@implementation LightBrick

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor blueColor];
    }
    return self;
}
- (NSUInteger)maxCollisions{
    return 3;
}

@end
