//
//  Brick.h
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Ball.h"
@interface Brick : UIView
@property (readonly,nonatomic) NSUInteger maxCollisions;
@property (readonly,nonatomic) NSUInteger collisions;
- (void)collideWithBall:(Ball *)ball;
- (void)destroy;
@end
NSString *const brickWasDestroyed;
