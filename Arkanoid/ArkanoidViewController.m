//
//  ArkanoidViewController.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import "ArkanoidViewController.h"
#import "GameViewController.h"

@interface ArkanoidViewController ()

@end

@implementation ArkanoidViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initGame];
}

- (void)initGame{
    GameViewController *game=[[GameViewController alloc]init];
    [self addChildViewController:game];
    game.view.frame=self.view.bounds;
    [self.view addSubview:game.view];
    [game didMoveToParentViewController:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
