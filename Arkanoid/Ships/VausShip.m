//
//  VausShip.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import "VausShip.h"

@implementation VausShip

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 100, 20)];
    if (self) {
        self.backgroundColor=[UIColor redColor];
    }
    return self;
}
- (id)init{
    return  [self initWithFrame:CGRectZero];
}
@end
