//
//  Ball.h
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ball : UIView
@property (strong,nonatomic) NSTimer *updater;
@property (assign,nonatomic) CGPoint velocity;
- (id)init;
- (void)destroy;
@end
