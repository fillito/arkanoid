//
//  Ball.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import "Ball.h"
@interface Ball()

@end
@implementation Ball

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 20, 20)];
    if (self) {
        // Initialization code
        self.backgroundColor=[UIColor grayColor];
        self.velocity = CGPointMake(3, -3);
        self.clipsToBounds=YES;
        self.layer.cornerRadius=9;
    }
    return self;
}

- (id)init{
    return [self initWithFrame:CGRectZero];
}
- (void)setVelocity:(CGPoint)velocity{
    if (velocity.x==velocity.y) {
        velocity.y+=1;
    }
    _velocity=velocity;
}
- (void)destroy{
    [self.updater invalidate];
    self.updater=nil;
    [self removeFromSuperview];
}
@end
