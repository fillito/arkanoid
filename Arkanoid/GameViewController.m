//
//  GameViewController.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import "GameViewController.h"
#import "VausShip.h"
#import "Ball.h"
#import "Brick.h"
#import "LightBrick.h"

@interface GameViewController ()<UIAlertViewDelegate>
@property (strong,nonatomic) VausShip *ship;
@property (strong,nonatomic) NSMutableArray *bricks;
@property (strong,nonatomic) NSMutableArray *balls;
@property (assign,nonatomic) NSUInteger ballInsertions;
@property (assign,nonatomic) CGPoint shipOriginOnPanGestureInit;
@property (strong,nonatomic) UIPanGestureRecognizer *panGestureRecognizer;
@property (strong,nonatomic) UITapGestureRecognizer *tapGestureRecognizer;
@property (weak, nonatomic) UIAlertView *gameOverAlert;
@property (weak, nonatomic) UIAlertView *gameWinAlert;
@end

static const CGFloat kShipBottomSeparation=30;
static const CGFloat kBrickSeparation=2;
@implementation GameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.ballInsertions=0;
        self.bricks=[NSMutableArray array];
        self.balls=[NSMutableArray array];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(brickWasDestroyed:) name:brickWasDestroyed object:nil];
    }
    return self;
}
- (void)viewDidLoad{
    [super viewDidLoad];
    [self loadGestureRecognizers];
    [self initGame];
}
- (void)initGame{
    [self addBricks];
    [self addVausShip];
    [self addBall];
}
#pragma mark - Game Events
- (void)loseGame{
    [self showGameOverAlert];
}
- (void)showGameOverAlert{
    UIAlertView *gameOverAlert=[[UIAlertView alloc]initWithTitle:@"Game Over" message:@"" delegate:self cancelButtonTitle:@"Continue" otherButtonTitles: nil];
    self.gameOverAlert=gameOverAlert;
    [gameOverAlert show];
}
- (void)winGame{
    [self removeAllBalls];
    [self showGameWinAlert];
}
- (void)showGameWinAlert{
    UIAlertView *gameWinAlert=[[UIAlertView alloc]initWithTitle:@"You Win !!" message:@"" delegate:self cancelButtonTitle:@"Play again" otherButtonTitles: nil];
    self.gameWinAlert=gameWinAlert;
    [gameWinAlert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView==self.gameOverAlert) {
        [self removeAllBricks];
        [self initGame];
    }else if (alertView==self.gameWinAlert){
        [self initGame];
    }
}
#pragma mark - Gesture Recognizer
- (void)loadGestureRecognizers{
    self.panGestureRecognizer=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanGestureRecognizer:)];
    [self.view addGestureRecognizer:self.panGestureRecognizer];
    self.tapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGestureRecognizer:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
}
- (IBAction)handlePanGestureRecognizer:(UIPanGestureRecognizer *)gestureRecognizer{
    CGPoint panGesturePoint=[gestureRecognizer translationInView:self.view];
    if (gestureRecognizer.state==UIGestureRecognizerStateBegan) {
        self.shipOriginOnPanGestureInit=self.ship.frame.origin;
    }
    CGPoint newOrigin=CGPointMake(self.shipOriginOnPanGestureInit.x+panGesturePoint.x, self.ship.frame.origin.y);
    [self moveShipToPoint:newOrigin];
}
- (IBAction)handleTapGestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer{
    CGPoint tapGesturePoint=[gestureRecognizer locationInView:self.view];
    CGRect currentShipFrame=self.ship.frame;
    currentShipFrame.origin.x=tapGesturePoint.x-(currentShipFrame.size.width/2);
    [UIView animateWithDuration:0.15 animations:^{
        [self moveShipToPoint:currentShipFrame.origin];
    }];
}
#pragma mark - Bricks
- (void)addBricks{
    NSUInteger rowsOfBricks=4;
    NSUInteger rows=0;
    CGPoint currentBrickOrigin=CGPointZero;
    do {
        Brick *brick=nil;
        if (rows==1 || rows==3) {
            brick=[[LightBrick alloc]init];
        }else{
            brick=[[Brick alloc]init];
        }
        CGRect brickFrame=brick.frame;
        brickFrame.origin=currentBrickOrigin;
        brick.frame=brickFrame;
        [self.view addSubview:brick];
        [self.bricks addObject:brick];
        currentBrickOrigin.x+=brickFrame.size.width+kBrickSeparation;
        CGRect nextBrickFrame=brickFrame;
        nextBrickFrame.origin=currentBrickOrigin;
        if (!CGRectContainsRect(self.view.bounds, nextBrickFrame)) {
            currentBrickOrigin.x=0;
            currentBrickOrigin.y+=brickFrame.size.height+kBrickSeparation;
            rows++;
        }
    } while (rows<rowsOfBricks);
}
- (void)removeAllBricks{
    for (Brick *brick in [self.bricks copy]) {
        [self.bricks removeObject:brick];
        [brick removeFromSuperview];
    }
}
- (void)brickWasDestroyed:(NSNotification *)notification{
    Brick *brick=notification.object;
    [self.bricks removeObject:brick];
    if (!self.bricks.count) {
        [self winGame];
    }
}
#pragma mark - Ship
- (void)addVausShip{
    if (!self.ship) {
        self.ship=[[VausShip alloc]init];
        [self.view addSubview:self.ship];
    }
    [self moveShipToInitialPosition];
}
- (void)moveShipToInitialPosition{
    CGRect shipFrame=self.ship.frame;
    shipFrame.origin.x=CGRectGetMidX(self.view.bounds)-(shipFrame.size.width/2);
    shipFrame.origin.y=CGRectGetMaxY(self.view.bounds)-shipFrame.size.height-kShipBottomSeparation;
    self.ship.frame=shipFrame;
}
- (void)moveShipToPoint:(CGPoint)newOrigin{
    CGRect movementFrame=CGRectMake(newOrigin.x, newOrigin.y, self.ship.frame.size.width, self.ship.frame.size.height);
    if (!CGRectContainsRect(self.view.bounds, movementFrame)) {
        if (movementFrame.origin.x<0) {
            movementFrame=CGRectMake(0, self.ship.frame.origin.y, self.ship.frame.size.width, self.ship.frame.size.height);
        }else{
            movementFrame=CGRectMake(CGRectGetMaxX(self.view.bounds)-self.ship.frame.size.width, self.ship.frame.origin.y, self.ship.frame.size.width, self.ship.frame.size.height);
        }
    }
    self.ship.frame=movementFrame;
}
#pragma mark - Ball
- (Ball *)addBall{
    Ball *ball=[[Ball alloc]init];
    ball.center=self.view.center;
    [self.view addSubview:ball];
    NSTimer *ballUpdater=[NSTimer scheduledTimerWithTimeInterval:0.016 target:self selector:@selector(updateBallPosition:) userInfo:ball repeats:YES];
    [ballUpdater fire];
    ball.updater=ballUpdater;
    [self.balls addObject:ball];
    self.ballInsertions+=1;
    return ball;
}
- (void)updateBallPosition:(NSTimer *)timer{
    Ball *ball=(Ball *)timer.userInfo;
    ball.center=CGPointMake(ball.center.x+ball.velocity.x, ball.center.y+ball.velocity.y);
    [self checkScreenCollisionForBall:ball];
    [self checkShipCollisionForBall:ball];
    [self checkBricksCollisiontForBall:ball];
}
- (void)removeAllBalls{
    for (Ball *ball in [self.balls copy]) {
        [self.balls removeObject:ball];
        [ball destroy];
    }
}
#pragma mark - Collisions
- (void)checkScreenCollisionForBall:(Ball *)ball{
    if (!CGRectContainsRect(self.view.bounds, ball.frame)) {
        if (CGRectGetMaxY(ball.frame)>self.view.bounds.size.height) {
            [ball destroy];
            [self.balls removeObject:ball];
            if (!self.balls.count) {
                [self loseGame];
            }
        }else{
            if (ball.frame.origin.x<0 || CGRectGetMaxX(ball.frame)>self.view.bounds.size.width) {
                ball.velocity=CGPointMake(-ball.velocity.x, ball.velocity.y);
            }
            if (ball.frame.origin.y<0) {
                ball.velocity=CGPointMake(ball.velocity.x, -ball.velocity.y);
            }
        }
    }
    ball.center=CGPointMake(ball.center.x+ball.velocity.x, ball.center.y+ball.velocity.y);
}
- (void)checkShipCollisionForBall:(Ball *)ball{
    if (CGRectIntersectsRect(self.ship.frame, ball.frame)) {
        CGRect intersection=CGRectIntersection(self.ship.frame, ball.frame);
        if (intersection.size.width>intersection.size.height) {
            ball.velocity=CGPointMake(ball.velocity.x, -ball.velocity.y);
        }else{
            ball.velocity=CGPointMake(-ball.velocity.x, ball.velocity.y);
        }
        ball.center=CGPointMake(ball.center.x+ball.velocity.x, ball.center.y+ball.velocity.y);
    }
}
- (void)checkBricksCollisiontForBall:(Ball *)ball{
    for (Brick *brick in [self.bricks copy]) {
        [self checkBrick:brick collistionWithBall:ball];
    }
}
- (void)checkBrick:(Brick *)brick collistionWithBall:(Ball *)ball{
    if (CGRectIntersectsRect(brick.frame, ball.frame)) {
        CGRect intersection=CGRectIntersection(brick.frame, ball.frame);
        if (intersection.size.width>intersection.size.height) {
            ball.velocity=CGPointMake(ball.velocity.x, -ball.velocity.y);
        }else{
            ball.velocity=CGPointMake(-ball.velocity.x, ball.velocity.y);
        }
        ball.center=CGPointMake(ball.center.x+ball.velocity.x, ball.center.y+ball.velocity.y);
        [brick collideWithBall:ball];
    }
}

#pragma mark - Memory Management
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
