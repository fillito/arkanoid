//
//  main.m
//  Arkanoid
//
//  Created by Daniel García García on 08/09/13.
//  Copyright (c) 2013 Daniel García García. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
